package com.example.buysjcwilfer.modelos;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.buysjcwilfer.MainActivity;
import com.example.buysjcwilfer.R;

import java.util.List;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ItemViewHolder> {

    private Context context;
    private List<Product> products;
    private ProductAdapterListener listener;

    public ProductAdapter(Context context, List<Product> products, ProductAdapterListener listener) {
        this.context = context;
        this.products = products;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ProductAdapter.ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View template = inflater.inflate(R.layout.template, parent, false);
        return new ProductAdapter.ItemViewHolder(template);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductAdapter.ItemViewHolder holder, int position) {

        final Product product = products.get(position);
        float buyTotal = product.getQuantity()*product.getPrice();
        holder.tv_product.setText(product.getProduct());
        holder.tv_quantity.setText(String.valueOf(product.getQuantity()));
        holder.tv_price.setText(String.valueOf(product.getPrice()));
        holder.tv_buyTotal.setText(String.valueOf(buyTotal));
        holder.iv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.deleteProduct(product.getId());
            }
        });
        holder.iv_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.editProduct(product.getId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    public void updateProducts(List<Product> products){
        this.products = products;
        notifyDataSetChanged();
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_product;
        private TextView tv_quantity;
        private TextView tv_price;
        private TextView tv_buyTotal;
        private ImageView iv_edit;
        private ImageView iv_delete;



        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);

        tv_product = itemView.findViewById(R.id.tv_product);
        tv_quantity = itemView.findViewById(R.id.tv_quantity);
        tv_price = itemView.findViewById(R.id.tv_price);
        tv_buyTotal = itemView.findViewById(R.id.tv_buyTotal);
        iv_edit = itemView.findViewById(R.id.iv_edit);
        iv_delete = itemView.findViewById(R.id.iv_delete);

        }
    }

    public interface ProductAdapterListener{
        void deleteProduct(String id);

        void editProduct(String id);
    }
}