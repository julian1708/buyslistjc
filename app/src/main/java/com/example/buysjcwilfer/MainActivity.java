package com.example.buysjcwilfer;

import android.content.Intent;
import android.os.Bundle;

import com.example.buysjcwilfer.modelos.Product;
import com.example.buysjcwilfer.modelos.ProductAdapter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import io.realm.Realm;

public class MainActivity extends AppCompatActivity implements ProductAdapter.ProductAdapterListener{

    private RecyclerView recyclerView;
    private Realm realm;
    private ProductAdapter productAdapter;
    private LinearLayout linearLayoutNull;
    private LinearLayout linearLayoutRecycler;
    private TextView tvValueTotal;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                irAOtraPantalla();
            }
        });

        realm = Realm.getDefaultInstance();
        linearLayoutNull = findViewById(R.id.llViewNull);
        linearLayoutRecycler = findViewById(R.id.llrecyclerView);
        recyclerView = findViewById(R.id.recyclerView);

        listSize();
        loadProducts();
    }

    private void listSize() {
        List<Product> products = getProducts();

        if (products.size() == 0){
            linearLayoutNull.setVisibility(View.VISIBLE);
            linearLayoutRecycler.setVisibility((View.GONE));
        }else{
            linearLayoutNull.setVisibility(View.GONE);
            linearLayoutRecycler.setVisibility((View.VISIBLE));
        }
    }

    @Override
    protected void onRestart(){
        super.onRestart();
        updateProducts();
    }

    private List<Product> getProducts(){
        tvValueTotal = findViewById(R.id.tv_value_total);
        List<Product> products = realm.where(Product.class).findAll();
        float cont = 0;
        for (int i = 0; i<products.size(); i++) {
            Product product = products.get(i);
            if (product != null){
                cont = cont + (product.getQuantity()*product.getPrice());
            }
        }
        tvValueTotal.setText(String.valueOf(cont));
        return products;
    }

    private void loadProducts(){
        List<Product> products =getProducts();
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL));
        productAdapter = new ProductAdapter(this, products, this);
        recyclerView.setAdapter(productAdapter);
    }

    @Override
    public void editProduct(String id){
        Intent intent =new Intent(this, EditProduct.class);
        intent.putExtra("productId", id);
        startActivity(intent);
    }

    private void updateProducts(){
        productAdapter.updateProducts(getProducts());
    }


    @Override
    public void deleteProduct(String id){
        realm.beginTransaction();
        Product product = realm.where(Product.class).equalTo("id", id).findFirst();
        if (product != null){
            product.deleteFromRealm();
        }
        realm.commitTransaction();
        Toast.makeText(MainActivity.this, "Eliminado", Toast.LENGTH_LONG).show();
        updateProducts();
        listSize();
    }


    private void irAOtraPantalla() {
        Intent intent = new Intent( this, AddProduct.class);
        startActivity(intent);
    }




}
