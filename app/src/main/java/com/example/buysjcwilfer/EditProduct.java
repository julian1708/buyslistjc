package com.example.buysjcwilfer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.buysjcwilfer.modelos.Product;

import java.util.UUID;

import io.realm.Realm;

public class EditProduct extends AppCompatActivity {

    private EditText etProduct;
    private EditText etDescription;
    private EditText etQuantity;
    private EditText etPrice;
    private Button btnSave;

    private Realm realm;

    private String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_product);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Editar producto");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getApplicationContext(),MainActivity.class));
                }
            });
        }
        realm = Realm.getDefaultInstance();
        loadViews();
        id = getIntent().getStringExtra("productId");
        loadProduct(id);
    }

    private void loadProduct(String id) {
        Product product = realm.where(Product.class).equalTo("id", id).findFirst();
        if (product != null) {
            etProduct.setText(product.getProduct());
            etDescription.setText(product.getDescription());
            etQuantity.setText(String.valueOf(product.getQuantity()));
            etPrice.setText(String.valueOf(product.getPrice()));
        }
    }

    private void loadViews() {
        etProduct = findViewById(R.id.et_product);
        etDescription = findViewById(R.id.et_description);
        etQuantity = findViewById(R.id.et_quantity);
        etPrice = findViewById(R.id.et_price);
        btnSave = findViewById(R.id.btn_save);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateProduct();
            }
        });
    }

    private void updateProduct() {
        String nameProduct = etProduct.getText().toString();
        String description = etDescription.getText().toString();
        String strQuantity = etQuantity.getText().toString().trim();
        String strPrice = etPrice.getText().toString().trim();
        boolean isSuccess = true;

        if (nameProduct.isEmpty()) {
            String message = getString(R.string.requeried);
            etProduct.setError(message);
            isSuccess = false;
        }
        if (strQuantity.isEmpty()) {
            String message = getString(R.string.requeried);
            etQuantity.setError(message);
            isSuccess = false;
        }
        if (strPrice.isEmpty()) {
            String message = getString(R.string.requeried);
            etPrice.setError(message);
            isSuccess = false;
        }
        if (isSuccess) {
            float price = Float.parseFloat(strPrice);
            int quantity = Integer.parseInt(strQuantity);
            Product product = new Product(id, nameProduct, description, quantity, (int) price);
            updateProductInDB(product);
            finish();
        }

    }

    private void updateProductInDB(Product product) {
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(product);
        realm.commitTransaction();
    }
}

